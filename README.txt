=== HAREMO Social Sharing ===
Contributors: haremo
Tags: social, sharing, og:image, opengraph, sharing, image, screenshot, webshot
Requires at least: 3.0.1
Tested up to: 4.5.3
Stable tag: 0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugins only task is to use the screenshot of your page as the Facebook's Open Graph image tag without you needing to do anything!

== Description ==

This plugins only task is to display the screenshot of the page as the Facebook's Open Graph image tag without you needing to do anything!
After installing, this plugin will produce screenshots for every page, these Screenshots will later be used as the image on Facebook.
You don't need to do anything!

Please be aware, that it can take a couple of minutes to produce the screenshots.

== The Screenshot is created on a Remote Server ==
Before the screenshot is created we send the URL of your page (and really JUST the URL!) to our API (api.social-screenshots.haremo.de), this API is responsible for creating the Screenshot.
As soon as the Screenshot is done, we send back a link with the image file to your page and it will be automatically be used as the Open Graph Image tag
This Image is being updated every 24 hours by our servers.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Unzip the `social-screenshots.zip` file
1. Upload `social-screenshots` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Does the Plugin cost anything? =

No!

== Screenshots ==

1. The Link to: [Agile Softwareentwicklung](https://haremo.de/agile-softwareentwicklung/) will look tike this

== Changelog ==

= 0.1 =
initial version of the plugin
